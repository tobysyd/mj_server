--[[
https://github.com/yongkangchen/poker-server

Copyright (C) 2016  Yongkang Chen lx1988cyk#gmail.com

GNU GENERAL PUBLIC LICENSE
   	Version 3, 29 June 2007

Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
Everyone is permitted to copy and distribute verbatim copies
of this license document, but changing it is not allowed.
--]]

return {
    START = 0x0015,
    ADD = 0x0016,
    OUT_GANG = 0x0018,
    GANG = 0x0021,
    PENG = 0x0022,
    PENG_GANG = 0x0026,
    OUT = 0x0017,
    SELECT = 0x0019,
    START_OUT = 0x1006,
    OUT_EXG_CARD = 0x0023,
    
    --------ZZ_MJ----------

    ZZ_OUT = 0x2017,
    ZZ_OUT_GANG = 0x2018,
    ZZ_HZ_HU = 0x2019,
    ZZ_HU = 0x2020,
    ZZ_GANG = 0x2021,
    ZZ_PENG = 0x2022,
    ZZ_PENG_GANG = 0x2026,

    ZZ_CHK_MASTER = 0x2027,
    ZZ_3CARDS = 0x2028,
    ZZ_QUE = 0x2029,
    ZZ_QUE_RET = 0x2030,
    ZZ_3CARDS_RET = 0x2031,
    ZZ_HU_RET = 0x2032,
    ZZ_FISH = 0x2033,
    ZZ_FISH_OVER = 0x2034,
    ------------------------
}
