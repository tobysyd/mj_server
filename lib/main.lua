#!/usr/local/bin/luajit

--[[
https://github.com/yongkangchen/poker-server

Copyright (C) 2016  Yongkang Chen lx1988cyk#gmail.com

GNU GENERAL PUBLIC LICENSE
   	Version 3, 29 June 2007

Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
Everyone is permitted to copy and distribute verbatim copies
of this license document, but changing it is not allowed.
--]]

local port = tonumber(arg[1])
local game_name = arg[2]

if not port or not game_name then
	print("usage: ./lib/main.lua port game_name")
	os.exit()
	return
end

package.cpath = package.cpath .. ";./libc/" .. jit.os .."/?.so"
package.path = package.path .. ";./lib/?.lua;./lib/net/?.lua;./src/?.lua;./lib/lobby/?.lua;./lib/lobby/lib/?.lua;./lib/simple/?.lua"

require "ext"

local log = require "log"
local LLOG = log.log
local LERR = log.error
local LTRACE = log.trace
local debug_traceback = debug.traceback

local function msg_send(client, ... )
	if client.fd == nil then
		return
	end
	client:write(table.dump{...} .. "\r\n")
end

do
    local msg = require "msg"
	local val_tbl = {}
    for k, v in pairs(require "game_msg") do
        if msg[k] == nil then
			if val_tbl[v] then
				LERR("!!! duplicate val in game_msg, k: %s => cur: %s, v: 0x%08x", k, val_tbl[v], v)
			end
            msg[k] = v
			val_tbl[v] = k
        else
            LERR("!!! duplicate key in game_msg, k: %s, v: 0x%08x => cur: 0x%08x", k, v, msg[k])
        end
    end
end

math.randomseed(os.time())

MSG_REG = MSG_REG or { }

require "login"
require "room"(game_name)

local function msg_handle(agent, pt, ...)
	local func = MSG_REG[ pt ]
	if func == nil then
		LERR("unknow pack, type: 0x%08x, sid: 0x%08x", pt, agent.sid or 0)
		return
	end
	LTRACE("recv msg, pid: %d, type: 0x%08x, %s", agent.id or 0, pt, table.dump{...})
	
    if pt > 0x0010 and agent.id == nil then
        LERR("invalid pack, type: 0x%08x, sid: 0x%08x", pt, agent.sid or 0)
        return
    end
	func(agent, ...)
end


function mytest(  )
	local do_check_hu = require "hu"
    local do_check_hu_type = require "hu_type"

    local dict = {}
    local card = 20

    --local  cards = {0,0,1,1,10,10,10,10,15,15,17,17,20,20} --long qi dui
    --local  cards = {0,0,1,1,3,3,4,4,6,6,7,7,2,2} -- qing long qi dui
    local cards = { [1] = 9,[2] = 9,[3] = 10,[4] = 10,[5] = 15,[6] = 15,[7] = 17,
    [8] = 17,[9] = 17,[10] = 19,[11] = 19,[12] = 23,[13] = 23,[14] = 17} 
    for _, v in ipairs(cards) do
        dict[v] = (dict[v] or 0) + 1
    end
    
    print( do_check_hu_type.get_hu_type(cards,{}) )
    os.exit(0)

    if card then
        --dict[card] = (dict[card] or 0) + 1
    end
    print( table.dump(dict) )

    local results = do_check_hu(dict)
    print("before \n")    
    print( table.dump(results) )
    --[[
    if results and #results == 1 and results[1] == 0x4000000 and not seven_hu then
        results = nil
    end    
    print("after\n")
    print( table.dump(results) )
    ]]
end

LLOG("listen: 0.0.0.0: %d", port)
--mytest()
--os.exit(0)


require "tcp_svr".start("0.0.0.0", port, function(client)
	coroutine.wrap(function()
        LLOG("accept, fd: %s, ip: %s, port: %s", client.fd, client.ip, client.port)
        




        client.send = msg_send
        client.agent = client
        
		while true do
            local msg = table.undump(client:read_line())
			local size = table.maxn(msg)
			if size < 256 then
				local ok, err = xpcall(msg_handle, debug_traceback, client.agent, unpack(msg, 1, size))
				if not ok then
					LERR("handler error: %s", debug_traceback(err))
				end	
			else
				LERR("invalid size: " .. size)
			end
		end
	end)()
end)
